import * as cdk from 'aws-cdk-lib';
import { Stack } from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';
import { WebsiteStack } from './website';

export class PortfolioSiteStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    // example resource
    // const queue = new sqs.Queue(this, 'PortfolioSiteQueue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });
    new WebsiteStack(this, 'WebsiteStack', {
      env: { account: Stack.of(this).account, region:Stack.of(this).region },
      stackName: 'WebsiteStack',
      // siteSubDomain: "beta.fms-api",
      domainName: "a-myers.com"
    });
  }
}
